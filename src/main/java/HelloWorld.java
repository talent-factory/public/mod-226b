/**
 * Dies ist nur eine Testklasse zur Überprüfung der Installation und der
 * Kommunikation mit meinem GitLab Repository.
 */
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello, World.");
    }
}
