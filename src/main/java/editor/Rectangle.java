package editor;

import lombok.Getter;
import lombok.ToString;

@SuppressWarnings("unused")
@Getter
@ToString
public class Rectangle extends Figure {

    /**
     * Länge und Breite des Rechteckes.
     */
    private final int height, width;

    /**
     * Erstellen eines neuen Rechteckes.
     *
     * @param height Länge des Rechteckes
     * @param width Breite des Rechteckes
     */
    public Rectangle(int height, int width) {
        this(new Position(), height, width);
    }

    /**
     * Erstellen eines neuen Rechteckes. Die Position [x,y] entspricht
     * der oberen, linken Ecke des Rechteckes.
     *
     * @param position Position des Rechteckes auf dem Display
     * @param height Länge des Rechteckes
     * @param width Breite des Rechteckes
     */
    public Rectangle(Position position, int height, int width) {
        super(position);
        this.height = height;
        this.width = width;
    }

    /**
     * Erstellen eines neuen Rechteckes. Die Position [x,y] entspricht
     * der oberen, linken Ecke des Rechteckes.
     *
     * @param x      Position des Rechteckes auf dem Display
     * @param y      Position des Rechteckes auf dem Display
     * @param height Länge des Rechteckes
     * @param width  Breite des Rechteckes
     */
    public Rectangle(int x, int y, int height, int width) {
        this(new Position(x, y), height, width);
    }

    /**
     * Zeichnet eine Figur auf dem Display.
     */
    @Override
    public void draw() {
        System.out.println(this);
    }
}
