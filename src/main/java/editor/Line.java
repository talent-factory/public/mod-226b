package editor;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Line extends Figure {

    /**
     * Endposition der Linie auf dem Display.
     */
    private final Position end;

    /**
     * Erstellen einer neuen Linie. Die Position [x,y] entspricht der
     * Startposition der Linie auf dem Display.
     *
     * @param start Startposition der Linie auf dem Display
     * @param end Endposition der Linie auf dem Display
     */
    public Line(Position start, Position end) {
        super(start);
        this.end = end;
    }

    /**
     * Erstellen einer neuen Linie. Die Position [x,y] entspricht der
     * Startposition der Linie auf dem Display.
     *
     * @param x    Startposition der Linie auf dem Display
     * @param y    Startposition der Linie auf dem Display
     * @param endX Endposition der Linie auf dem Display
     * @param endY Endposition der Linie auf dem Display
     */
    public Line(int x, int y, int endX, int endY) {
        this(new Position(x, y), new Position(endX, endY));
    }

    /**
     * Zeichnet eine Figur auf dem Display.
     */
    @Override
    public void draw() {
        System.out.println(this);
    }
}
