package patterns.state;

import lombok.Setter;

public class Dialog {

    @Setter
    private Tool tool;

    public void mouseDown() {
        tool.mouseDown();
    }

    public void mouseUp() {
        tool.mouseUp();
    }
}
