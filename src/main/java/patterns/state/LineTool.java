package patterns.state;

public class LineTool implements Tool {

    @Override
    public void mouseDown() {
        System.out.println("Line icon");
    }

    @Override
    public void mouseUp() {
        System.out.println("Draw a line");
    }
}
