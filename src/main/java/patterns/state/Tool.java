package patterns.state;

/**
 * Dieses Interface ermöglicht verschiedenen Werkzeugen des
 * Grafikeditors die individuelle Umsetzung der aufgeführten
 * Methoden.
 */
public interface Tool {

    void mouseDown();

    void mouseUp();
}
