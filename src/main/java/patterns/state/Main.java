package patterns.state;

public class Main {

    public static void main(String[] args) {

        var dialog = new Dialog();

        dialog.setTool(new RactangleTool());
        dialog.mouseDown();
        dialog.mouseUp();
    }
}
