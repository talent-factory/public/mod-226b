# State

The State Pattern allows an object to alter its behavior when its internal state changes. The object will appear to change its class.

## Bullet points

- The State Pattern allows an object to have many different behaviors that are based on its internal state.
- Unlike a procedural state machine, the State Pattern represents state as a full-blown class.
- The Context gets its behavior by delegating to the current state object it is composed with.
- By encapsulating each state into a class, we localize any changes that will need to be made.

![UML](State.png)
