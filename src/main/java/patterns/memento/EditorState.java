package patterns.memento;

import lombok.Getter;

/**
 * Speichert den aktuellen Zustand des Editors.
 */
public class EditorState {

    @Getter
    private final String content;

    /**
     * Erstellt ein neues Zustandsobjekt des Editors.
     *
     * @param content aktueller Zustand
     */
    public EditorState(String content) {
        this.content = content;
    }
}
