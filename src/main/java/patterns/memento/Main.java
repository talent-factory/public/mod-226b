package patterns.memento;

/**
 * Mit dieser Hilfsklasse zeigen wir die Funktionalität des
 * <a href="https://java-design-patterns.com/patterns/memento/">Memento</a>
 * Entwurfsmusters.
 */
public class Main {

    public static void main(String[] args) {

        /*
         * Ganz im Sinne des Single Responsibility Principle erledigen
         * die folgenden Objekte nur EINE Aufgabe.
         */
        Editor editor = new Editor();    // Editierfunktionen
        History history = new History(); // Verwalten der 'History'

        // Definieren eines neuen Inhaltes und verwalten des
        // aktuellen Zustandes.
        editor.setContent("a");
        history.push(editor.createState());

        // Definieren eines neuen Inhaltes und verwalten des
        // aktuellen Zustandes.
        editor.setContent("b");
        history.push(editor.createState());

        // Definieren eines neuen Inhaltes. Anschliessend machen
        // wir diese Änderung wieder rückgängig.
        editor.setContent("c");
        System.out.println(editor); // Ausgabe von 'c'

        editor.undo(history.pop());
        System.out.println(editor); // Ausgabe von 'b'

        editor.undo(history.pop());
        System.out.println(editor); // Ausgabe von 'a'      
    }
}
