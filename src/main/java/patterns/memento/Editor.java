package patterns.memento;

import lombok.Setter;
import lombok.ToString;

/**
 * Mit diesem Editor wollen wir das
 * <a href="https://java-design-patterns.com/patterns/memento/">Memento</a>
 * Entwurfsmuster erläutern. Mit diesem Editor wollen die eine 'Undo' Funktion
 * einbauen (<i>restore an a previous state</i>).
 */
@ToString
public class Editor {

    @Setter
    private String content;

    /**
     * Erstellt einen neuen Zustand des Editors mit dem aktuellen
     * Inhalt.
     *
     * @return aktueller Zustand des Editors
     */
    public EditorState createState() {
        return new EditorState(content);
    }

    public void undo(EditorState state) {
        content = state.getContent();
    }
}
