# Memento

Use the Memento Pattern when you need to be able to return an object to one of its previous states; for instance, if your user requests an “undo.”

## The Memento at work

The Memento has two goals:

- Saving the important state of a system’s key object.
- Maintaining the key object’s encapsulation.

Keeping the single responsibility principle in mind, it’s also a good idea to keep the state that you’re saving separate from the key object. This separate object that holds the state is known as the Memento object.

![UML](Memento.png)
